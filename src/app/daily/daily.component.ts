import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../weather.service';
import { ApiService } from '../api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-daily',
  templateUrl: './daily.component.html',
  styleUrls: ['./daily.component.css']
})
export class DailyComponent implements OnInit {
  weathers = [];
  
  constructor(private api: WeatherService , private router : Router) {}
  
  ngOnInit() {
    this.api.getWeather().subscribe(heroes => {
      this.weathers = heroes.list
      console.log(this.weathers);
    });
  }

  getData(weather)
  {
    this.api.setData(weather)
    console.log('daily component'+weather.dt)
    this.router.navigate(['/detail/'])
  }

}


