import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  constructor(private http:HttpClient) { }

  getWeather(): Observable<any>
  {
    return this.http.get<any>("https://samples.openweathermap.org/data/2.5/forecast/hourly?id=524901&appid=b6907d289e10d714a6e88b30761fae22");
  }
  climate : any
  setData(weather)
  {
    this.climate = weather
  }

  display()
  {
  return this.climate
  }
}
