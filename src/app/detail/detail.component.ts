import { Component, OnInit } from '@angular/core';
import {WeatherService} from '../weather.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  constructor(private weatherService : WeatherService) { }
  response :any
  ngOnInit() {
    this.response = this.weatherService.display()
    console.log('detail component'+this.response.dt)
  }

}
