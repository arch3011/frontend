import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {WeatherService} from '../app/weather.service';
import { DailyComponent } from './daily/daily.component';
import { DetailComponent } from './detail/detail.component';

const routes: Routes = [
  { path: '', component: DailyComponent },
  { path: 'detail', component: DetailComponent }
];
@NgModule({
  declarations: [],
  imports: [
    [RouterModule.forRoot(routes)],
   
  ],exports: [RouterModule]
})
export class AppRoutingModule { }
